import Vue from 'vue'
import App from './App.vue'
import router from './router'
import {store} from './store/store'

Vue.config.productionTip = true

new Vue({
  store: store,
  router,
  render: h => h(App)
}).$mount('#app')


Vue.mixin({
  data: function(){
    return {
      apiAddress: 'http://localhost:8081/api/'
    }
  }
})
