import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home
  },
  {
    path: '/users',
    name: 'users',
    component: () => import(/* webpackChunkName: "users" */ '../views/Users.vue')
  },
  {
    path: '/new-customer/:user_id',
    name: 'new customer',
    component: () => import(/* webpackChunkName: "newcustomer" */ '../views/NewCustomer.vue')
  },
  {
    path: '/new-deposit',
    name: 'new deposit',
    component: () => import(/* webpackChunkName: "newcustomer" */ '../views/NewDeposit.vue')
  },
  {
    path: '/new-payment',
    name: 'new payment',
    component: () => import(/* webpackChunkName: "newtransfer" */ '../views/NewTransfer.vue')
  },
  {
    path: '/login',
    name: 'login',
    component: () => import(/* webpackChunkName: "login" */ '../views/Login.vue')
  },
  {
    path: '/deposits',
    name: 'deposits',
    component: () => import(/* webpackChunkName: "deposits" */ '../views/DepositList.vue')
  },
  {
    path: '/deposit/:deposit_id',
    name: 'deposit',
    component: () => import(/* webpackChunkName: "deposit" */ '../views/DepositInfo.vue')
  },
  {
    path: '/new-credit',
    name: 'new credit',
    component: () => import(/* webpackChunkName: "newcredit" */ '../views/NewCredit.vue')
  },
  {
    path: '/test-view',
    name: 'Test View',
    component: () => import(/* webpackChunkName: "testview" */ '../views/TestView.vue')
  }
]

const router = new VueRouter({
  routes
})

// const isAuthenticated=true;

router.beforeEach((to, from, next) => {
  if (!router.app.$store.state.isAuthenticated && to.path!='/login') next('/login')
  else next()
})


export default router
