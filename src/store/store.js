import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export const store = new Vuex.Store({
  state: {
    isAuthenticated: false,
    username: null,
    password: null,
    notifications: []
  },
  mutations: {
    authenticate(state) {
      state.isAuthenticated = true;
    },
    setUsername(state, newUsername) {
      state.username = newUsername;
    },
    setPassword(state, newPassword) {
      state.password = newPassword;
    },
    newNotification(state, status, message) {
      state.notifications.push({
        status: status,
        message: message
      });
    },
    removeNotification(state, index) {
      state.notifications.splice(index, 1);
    },
    addNotifications(state, response) {
      const notifications = response.data.notifications;
      for (const key in notifications) {
        state.notifications.push({
          status: notifications[key].status,
          message: notifications[key].message
        });
      }
    }
  }
});
